# pingapp

This is a basic ASP.Net Core web application with a /ping endpoint that returns 'pong' when you browse to it in a web browser.
The pipeline script builds and deploys the web application to AWS's ECS in a docker container.

## Tools

- Visual studio for building the ping endpoint.
- Docker for creating images and containerizing the app.
- Bitbucket Pipelines for automation.
- AWS ECS to manage containers in the cloud.

## Successful steps

- Built the ping app and ran it locally.
- Containerized the app and ran locally.
- Created a an AWS ECS repository with the docker image.
- Created the ECS infrastructure (Cluster, Service and Task definition) which provisions an EC2 instance exposing the ping endpoint publicly.
- Built a Bitbucket Pipeline which triggers a new build on each push and creates a new task with the changes on AWS ECS.
