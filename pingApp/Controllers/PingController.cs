﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace pingApp.Controllers
{
  [Produces("application/json")]
  [Route("Ping")]
  public class PingController : Controller
  {
    [HttpGet]
    public IActionResult Ping()
    {
      return Ok("Hello from IQmetrix!");
    }
  }
}
