# Builder stage
FROM mcr.microsoft.com/dotnet/core/sdk:3.0 AS builder-stage

# Set working directory
WORKDIR /app

# Copy project from the source to the docker container filesystem
COPY *.sln .
COPY pingApp/*.csproj ./pingApp/

# Install all Dependancies
RUN dotnet restore pingApp

# Copy all remaining files to our image
COPY ./ ./

# Publish the app
RUN dotnet publish pingApp/pingApp.csproj -c Release -o /app/out

# Define port
Expose 80

# Runtime stage
FROM mcr.microsoft.com/dotnet/core/aspnet:3.0 AS runtime-stage

WORKDIR /app
COPY --from=builder-stage /app/out .

# Entry point
ENTRYPOINT [ "dotnet" ]
CMD [ "pingApp.dll" ]